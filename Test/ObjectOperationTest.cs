﻿using System;
using jWhale_cs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class ObjectOperationTest
    {
        [TestMethod]
        public void ContainerPlayBackTest()
        {
            Operation samplePlayBack = new ContainerPlayback()
                .Target("test")
                .SetOperation(Playback.RESTART)
                .ConfigOperation();
            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                new ContainerPlayback().ConfigOperation();
            });
            Assert.AreEqual(Method.POST, samplePlayBack.MethodName);
            Assert.AreNotEqual(true, samplePlayBack.LastParam.IsEmpty);
        }

        [TestMethod]
        public void ContainerRemoveTest()
        {
            Operation sampleRemove = new ContainerRemove()
                .Target("test")
                .ConfigOperation();
            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                new ContainerRemove().ConfigOperation();
            });
            Assert.AreNotEqual(true, sampleRemove.LastParam.IsEmpty);
            Assert.AreEqual(Method.DELETE, sampleRemove.MethodName);
            Assert.AreEqual("", sampleRemove.LastParam.Element);
        }

        [TestMethod]
        public void ContainerRenameTest()
        {
            ContainerRename sampleRename = new ContainerRename()
                .Target("test");
            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                sampleRename.ConfigOperation();
            });
            sampleRename.SetNewName("newName");
            Assert.AreEqual(Method.POST, sampleRename.ConfigOperation().MethodName);
        }

        [TestMethod]
        public void ItemRemoveTest()
        {
            IObjectOp<ImageDelete> sampleImage = new ImageDelete().Target("imageName");
            IObjectOp<NetworkRemove> sampleNetwork = new NetworkRemove().Target("networkName");
            IObjectOp<VolumeRemove> sampleVolume = new VolumeRemove().Target("volumeName");
            Assert.AreEqual(Method.DELETE, sampleImage.ConfigOperation().MethodName);
            Assert.AreNotEqual(Method.GET, sampleNetwork.ConfigOperation().MethodName);
            Assert.AreNotEqual(true, sampleVolume.ConfigOperation().PathParam.IsEmpty);

        }


    }
}
