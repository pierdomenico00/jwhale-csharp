﻿using System;
using jWhale_cs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class ListTest
    {
        [TestMethod]
        public void ListContainerTest()
        {
            ContainerList containers = new ContainerList().BaseSetup();
            string last = "/json";
            Assert.AreEqual(EndPoint.CONTAINER, containers.ConfigOperation().EndPointName);
            Assert.AreEqual(last, containers.ConfigOperation().LastParam.Element);
            Assert.AreNotEqual(true, containers.ConfigOperation().LastParam.IsEmpty);
            Assert.AreNotEqual(true, containers.ConfigOperation().QueryParams.IsEmpty);
            Assert.AreEqual(Method.GET, containers.ConfigOperation().MethodName);
        }

        [TestMethod]
        public void ListImageTest()
        {
            ImageList images = new ImageList().BaseSetup();
            string last = "/json";
            Assert.AreEqual(EndPoint.IMAGE, images.ConfigOperation().EndPointName);
            Assert.AreEqual(last, images.ConfigOperation().LastParam.Element);
            Assert.AreNotEqual(true, images.ConfigOperation().LastParam.IsEmpty);
            Assert.AreNotEqual(true, images.ConfigOperation().QueryParams.IsEmpty);
            Assert.AreEqual(Method.GET, images.ConfigOperation().MethodName);
        }

        [TestMethod]
        public void ListNetworkTest()
        {
            NetworkList networks = new NetworkList().BaseSetup();
            string last = "";
            Assert.AreEqual(EndPoint.NETWORK, networks.ConfigOperation().EndPointName);
            Assert.AreEqual(Method.GET, networks.ConfigOperation().MethodName);
            Assert.AreEqual(last, networks.ConfigOperation().LastParam.Element);
            Assert.AreNotEqual(true, networks.ConfigOperation().LastParam.IsEmpty);
            Assert.AreEqual(false, networks.ConfigOperation().QueryParams.IsEmpty);
        }

        [TestMethod]
        public void ListVolumeTest()
        {
            string last = "";
            VolumeList volumes = new VolumeList();
            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                volumes.ConfigOperation();
            });
            volumes.BaseSetup();
            Assert.AreEqual(EndPoint.VOLUME, volumes.ConfigOperation().EndPointName);
            Assert.AreEqual(last, volumes.ConfigOperation().LastParam.Element);
            Assert.AreEqual(Method.GET, volumes.ConfigOperation().MethodName);
            Assert.AreEqual(false, volumes.ConfigOperation().QueryParams.IsEmpty);
        }
    }
}
