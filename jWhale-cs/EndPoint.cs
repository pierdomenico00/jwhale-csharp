﻿using System;

namespace jWhale_cs
{
    public class EndPoint
    {
        public static String CONTAINER
        { get { return "/containers"; } }
        public static String VERSION
        { get { return "/version"; } }
        public static String BUILD
        { get { return "/build"; } }
        public static String IMAGE
        { get { return "/images"; } }
        public static String NETWORK
        { get { return "/networks"; } }
        public static String VOLUME
        { get { return "/volumes"; } }
    }
}
