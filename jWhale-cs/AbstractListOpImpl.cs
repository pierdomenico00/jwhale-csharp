﻿using System;

namespace jWhale_cs
{
    public abstract class AbstractListOpImpl<T> : IListOp<T>
    {
        private bool setup;
        private readonly Operation listOperation;

        public AbstractListOpImpl(Method method, string endPoint)
        {
            listOperation = new Operation(method, endPoint);
        }

        public Operation ConfigOperation()
        {
            if (!setup)
            {
                throw new InvalidOperationException();
            }
            return listOperation;
        }

        public abstract T BaseSetup();

        protected Operation GetOperation()
        {
            return listOperation;
        }

        protected void SetSetup()
        {
            setup = true;
        }
    }
}

