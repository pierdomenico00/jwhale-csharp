﻿using System;
using System.Collections.Generic;

namespace jWhale_cs
{
    public class Operation
    {
        public Optional<Dictionary<string, string>> QueryParams { get; set; } = new Optional<Dictionary<string, string>>(new Dictionary<string, string>());
        public Optional<string> PathParam { get; set; } = new Optional<string>();
        public Optional<string> RequestBody { get; set; } = new Optional<string>();
        public Optional<string> LastParam { get; set; } = new Optional<string>();
        public Method MethodName { get; }
        public String EndPointName { get; }

        public Operation(Method method, String endPoint)
        {
            this.MethodName = method;
            this.EndPointName = endPoint;
        }

        public void AddQueryParams(string key, string value)
        {
            QueryParams.Element[key] = value;
        }
    }
}
