﻿namespace jWhale_cs
{
    public sealed class ImageList : AbstractListOpImpl<ImageList>
    {
        private const string LAST = "/json";

        public ImageList() : base(Method.GET, EndPoint.IMAGE) { }

        public override ImageList BaseSetup()
        {
            GetOperation().LastParam = new Optional<string>(LAST);
            GetOperation().AddQueryParams("all", "1");
            SetSetup();
            return this;
        }
    }
}
