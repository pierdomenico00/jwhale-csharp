﻿namespace jWhale_cs
{
    public sealed class ContainerRename : AbstractObjectOpImpl<ContainerRename>
    {
        private const string RENAME = "/rename";
        private const string DEF_NAME = "";
        private const string NAME = "name";

        public ContainerRename() : base(Method.POST, EndPoint.CONTAINER) { }

        public override ContainerRename Target(string mandatoryParam)
        {
            GetOperation().AddQueryParams(NAME, DEF_NAME);
            GetOperation().LastParam = new Optional<string>(RENAME);
            GetOperation().PathParam = new Optional<string>("/" + mandatoryParam);
            return this;
        }

        public ContainerRename SetNewName(string newName)
        {
            GetOperation().AddQueryParams(NAME, newName);
            SetSetup();
            return this;
        }
    }
}
