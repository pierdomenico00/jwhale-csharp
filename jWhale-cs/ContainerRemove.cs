﻿namespace jWhale_cs
{
    public sealed class ContainerRemove : AbstractObjectOpImpl<ContainerRemove>
    {
        private const string EMPTY = "";

        public ContainerRemove() : base(Method.DELETE, EndPoint.CONTAINER) { }

        public override ContainerRemove Target(string mandatoryParam)
        {
            GetOperation().PathParam = new Optional<string>("/" + mandatoryParam);
            GetOperation().LastParam = new Optional<string>(EMPTY);
            SetSetup();
            return this;
        }
    }
}
