﻿namespace jWhale_cs
{
    public enum Method
    {
        GET,

        HEAD,

        POST,

        PUT,

        DELETE
    }
}
