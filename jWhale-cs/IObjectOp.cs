﻿using System;

namespace jWhale_cs
{
    public interface IObjectOp<T>
    {
        T Target(String mandatoryParam);

        Operation ConfigOperation();
    }
}
