﻿namespace jWhale_cs
{
    public sealed class NetworkList : AbstractListOpImpl<NetworkList>
    {
        private const string EMPTY = "";

        public NetworkList() : base(Method.GET, EndPoint.NETWORK) { }

        public override NetworkList BaseSetup()
        {
            GetOperation().LastParam = new Optional<string>(EMPTY);
            SetSetup();
            return this;
        }
    }
}
