﻿namespace jWhale_cs
{
    public sealed class ImageDelete : AbstractObjectOpImpl<ImageDelete>
    {
        public ImageDelete() : base(Method.DELETE, EndPoint.IMAGE) { }

        public override ImageDelete Target(string mandatoryParam)
        {
            GetOperation().LastParam = new Optional<string>(mandatoryParam);
            SetSetup();
            return this;
        }
    }
}
