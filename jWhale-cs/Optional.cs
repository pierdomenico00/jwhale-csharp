﻿using System;

namespace jWhale_cs
{
    public class Optional<T>
    {
        public bool IsEmpty;
        private T element;
        public T Element
        { get { if (!IsEmpty)
                {
                    return element;
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public Optional(T element)
        {
            this.element = element;
            IsEmpty = false;
        }

        public Optional()
        {
            IsEmpty = true;
        }

    }
}
