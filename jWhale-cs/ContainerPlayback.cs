﻿namespace jWhale_cs
{
    public sealed class ContainerPlayback : AbstractObjectOpImpl<ContainerPlayback>
    {
        public ContainerPlayback() : base(Method.POST, EndPoint.CONTAINER) { }

        public ContainerPlayback SetOperation(string operation)
        {
            GetOperation().LastParam = new Optional<string>(operation);
            SetSetup();
            return this;
        }

        public override ContainerPlayback Target(string mandatoryParam)
        {
            GetOperation().PathParam = new Optional<string>("/" + mandatoryParam);
            return this;
        }
    }
}
