﻿namespace jWhale_cs
{
    public sealed class NetworkRemove : AbstractObjectOpImpl<NetworkRemove>
    {
        public NetworkRemove() : base(Method.DELETE, EndPoint.NETWORK) { }

        public override NetworkRemove Target(string mandatoryParam)
        {
            GetOperation().PathParam = new Optional<string>("/" + mandatoryParam);
            SetSetup();
            return this;
        }
    }
}
