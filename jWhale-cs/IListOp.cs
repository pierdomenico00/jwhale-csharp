﻿namespace jWhale_cs
{
    interface IListOp<T>
    {
        Operation ConfigOperation();

        T BaseSetup();
    }
}
