﻿namespace jWhale_cs
{
    public sealed class VolumeList : AbstractListOpImpl<VolumeList>
    {
        private const string LAST = "";

        public VolumeList() : base(Method.GET, EndPoint.VOLUME) { }

        public override VolumeList BaseSetup()
        {
            GetOperation().LastParam = new Optional<string>(LAST);
            SetSetup();
            return this;
        }
    }
}
