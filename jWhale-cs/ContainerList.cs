﻿namespace jWhale_cs
{
    public sealed class ContainerList : AbstractListOpImpl<ContainerList>
    {
        private const string ALL_PARAM = "all";
        private const string NOT_RUNNING = "1";
        private const string LAST = "/json";

        public ContainerList() : base(Method.GET, EndPoint.CONTAINER) { }

        public override ContainerList BaseSetup()
        {
            GetOperation().AddQueryParams(ALL_PARAM, NOT_RUNNING);
            GetOperation().LastParam = new Optional<string>(LAST);
            SetSetup();
            return this;
        }
    }
}
