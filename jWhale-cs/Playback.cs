﻿namespace jWhale_cs
{
    public class Playback
    {
        public static string START
        { get { return "/start"; } }
        public static string STOP
        { get { return "/stop"; } }
        public static string RESTART
        { get { return "/restart"; } }
    }
}