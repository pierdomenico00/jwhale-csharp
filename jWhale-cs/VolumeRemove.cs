﻿namespace jWhale_cs
{
    public sealed class VolumeRemove : AbstractObjectOpImpl<VolumeRemove>
    {
        public VolumeRemove() : base(Method.DELETE, EndPoint.VOLUME) { }

        public override VolumeRemove Target(string mandatoryParam)
        {
            GetOperation().PathParam = new Optional<string>("/" + mandatoryParam);
            SetSetup();
            return this;
        }
    }
}
