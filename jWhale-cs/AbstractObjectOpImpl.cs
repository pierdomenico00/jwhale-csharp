﻿using System;
namespace jWhale_cs
{
    public abstract class AbstractObjectOpImpl<T> : IObjectOp<T>
    {
        private bool setup;
        private Operation objectOperation;

        public AbstractObjectOpImpl(Method method, string endPoint)
        {
            objectOperation = new Operation(method, endPoint);
        }

        public Operation ConfigOperation()
        {
            if (!setup)
            {
                throw new InvalidOperationException();
            }
            return objectOperation;
        }

        public abstract T Target(string mandatoryParam);

        protected Operation GetOperation()
        {
            return objectOperation;
        }

        protected void SetSetup()
        {
            setup = true;
        }
    }
}
